From python:3.6.8-slim

RUN apt-get update && apt-get install -y \
    build-essential \
    make \
    gcc \
    python3-dev \
    git \
    lsof

RUN mkdir -p /usr/share/man/man1
        
RUN apt-get update && apt-get install -y \
    default-jdk \
    procps

RUN mkdir /opt/share

WORKDIR /opt/share

RUN mkdir log && mkdir temp

ADD . .

RUN pip install -r requirements.txt

EXPOSE 8888 4040

CMD jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.iopub_data_rate_limit=1000000000000 
