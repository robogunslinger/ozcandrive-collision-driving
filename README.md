For ingesting static driving data and survey data to explore the relationship between collision, driving, functional and mental capabilities.

To run the demo branch in docker compose
- Install docker app on the machine
- Download the git repository 
- Navigate to the repository and run following command in the terminal to build the docker images
    - docker-compose build
- After the above docker image build, which would take some time depending on your network speed to download the docker images, run following command to run the container using docker-compose
    - docker-compose up
- After running the above command, the jupyter notebook link would be shown in the terminal. Copy and paste the link to a browser to start working on jupyter.
- To shut down, in the terminal that started the docker-compose, press "ctrl"+"c" from the keyboard. To clean up the contain, run docker-compose down after the "ctrl"+"c".
