# Functions for transformation
import pandas as pd
import numpy as np
import statistics 
import pyspark
from pyspark.sql import functions as f
from pyspark.sql import SparkSession
from pyspark.sql import types as t
import pyarrow.parquet as pq

from ozcandrive.util import file_util
from ozcandrive.util import spark_logging as logging
from ozcandrive.driving.driving import *

def get_collision_before_after_geohash(
    records: list, 
    record_id: int) \
    -> list:
    '''
    Function to get the adjacent geohash around the collision point
    '''
    another_before_prev_colli = {}
    prev_colli = {}
    after_colli = {}
    stage = None
    
    try:
        colli_row_num = record_id
        
        stage = 'Prev colli'
        filtered_records = list(filter(
            lambda x: x['row_number'] <= record_id, 
            records))
        colli_geohash = filtered_records[-1]['geohash']
        
        prev_colli = get_next_different(
            records=filtered_records,
            matching={'key': 'geohash', 'value': colli_geohash}, 
            returning=['row_number', 'geohash'], 
            reverse=True
        )
        prev_colli_row_num = prev_colli['row_number']
        prev_colli['row_number'] = str(prev_colli['row_number'])
        
        stage = 'Another before prev colli'
        filtered_records = list(filter(
            lambda x: x['row_number'] <= prev_colli_row_num, 
            filtered_records))
        
        another_before_prev_colli = get_next_different(
            records=filtered_records, 
            matching={'key': 'geohash', 'value': prev_colli['geohash']},
            returning=['row_number', 'geohash'], 
            reverse=True
        )
        another_before_prev_colli['row_number'] = str(another_before_prev_colli['row_number'])
        
        stage = 'After Colli'
        filtered_records = list(filter(
            lambda x: x['row_number'] >= record_id, 
            records))
        
        after_colli = get_next_different(
            records=filtered_records,
            matching={'key': 'geohash', 'value': colli_geohash},
            returning=['row_number', 'geohash']
        )
        after_colli['row_number'] = str(after_colli['row_number'])
        
    except:
        logger = logging.getLogger('executor')
        logger.warn(f'{stage} -- {str(filtered_records)}')
        
    return [
        another_before_prev_colli,
        prev_colli, 
        {'row_number': str(colli_row_num), 'geohash': colli_geohash},
        after_colli
    ]
 
get_collision_before_after_geohash_udf = f.udf(
    get_collision_before_after_geohash, 
    t.ArrayType(t.MapType(t.StringType(), t.StringType())))

def get_next_different(
    records: list, 
    matching: dict, 
    returning: list, 
    reverse=False) \
    -> dict:
    '''
    Function to get the next different value from a list 
    Return the key-value pair for row_number and geohash
    '''
    if reverse:
        # iterate from (length-1) to 0
        iter_range = range(len(records)-1, -1, -1)
    else:
        # iterate from 0 to (length-1)
        iter_range = range(len(records))
        
    result = {}
    adjacent_result = {}
    has_run_once = False
    has_found_next = False
    precedings = None
    
    for i in iter_range:   
        # temporarily store the adjacent one
        if not has_run_once:
            adjacent_result = records[i]
            has_run_once = True

        # if found the next different one, use it as the result and update the flag "has_found_next"              
        if records[i][matching['key']] != matching['value']:
            has_found_next = True
            for col in returning:
                result[col] = records[i][col]
            break

    if not has_found_next:
        for col in returning:
            result[col] = adjacent_result[col]         

    return result

def get_crash_trip_duration(
    records: list, 
    record_id: int) \
    -> int:
    '''
    Get the duration between the trip start and the crash moment in seconds
    by calculating the difference between the row number
    '''
    return record_id - records[0]['row_number']

get_crash_trip_duration_sec_udf = f.udf(get_crash_trip_duration, t.LongType())

def get_collision_moment_info(
    records: list, 
    record_id: int, 
    lat_col_name: str, 
    lon_col_name: str, 
    speed_col_name: str,
    speedlimit_col_name: str) \
    -> dict:
    matching = list(filter(lambda x: x['row_number'] == record_id , records))
    return {'lat_str': matching[0][lat_col_name],
            'lon_str': matching[0][lon_col_name],
            'speed': matching[0][speed_col_name],
            'speed_limit': matching[0][speedlimit_col_name]}

get_collision_moment_udf = f.udf(
    get_collision_moment_info, 
    t.MapType(t.StringType(), t.StringType()))
    
def get_collision_brake(
    records: list, 
    record_id: int, 
    col_name: str) \
    -> dict:
    '''
    Function to get the moment with the largest deceleration after the collision record (manually identified)
    '''
    following = list(filter(lambda x: (x['row_number'] >= record_id) and (x['row_number'] < record_id + 10) , records))
    
    speed_list = [convert_to_float(record[col_name]) for record in following]
    acc_list = np.diff(speed_list)
    idx = int(np.argmin(acc_list))
    
    return {'row_num': int(np.argmin(acc_list)) + record_id,
            'speed_kmh': str(speed_list[idx]),
            'deceleration_kmh_per_s': str(acc_list[idx])} 

get_collision_brake_udf = f.udf(
    get_collision_brake, 
    t.MapType(t.StringType(), t.StringType()))

def get_most_frequent(
    records: list, 
    col_name: str) \
    -> str:
    '''
    Function to get the mode from a list
    '''
    item_list = [ record[col_name] for record in records]
    return str(statistics.mode(item_list))

get_trip_serial_udf = f.udf(get_most_frequent, t.StringType())

def read_manual_identified_colli_info(
    spark: SparkSession,
    filename: str) \
    -> pyspark.sql.DataFrame:
    
    df = spark \
        .read.csv(
            filename, 
            header=True, 
            inferSchema=False) \
        .withColumnRenamed(
            'row_number', 
            'collision_row_num') \
        .withColumn(
            'collision_row_num', 
            f.col('collision_row_num').cast(t.LongType())) \
        .withColumn(
            'trip_confirmed_manually', 
            f.col('trip_confirmed_manually').cast(t.LongType()))
    
    return df


def join_colli_basics_with_summary_driving(
    colli_basic_df: pyspark.sql.DataFrame,
    summ_df: pyspark.sql.DataFrame) \
    -> pyspark.sql.DataFrame:
    '''
    Join the collision basic info, which was manually identified, with 
    the summary driving info
    '''
    join_cond = [
        colli_basic_df['pt_id'] == summ_df['file_pt_id'],
        colli_basic_df['actual_date_confirmed_manually'] == summ_df['date_str'],
        colli_basic_df['trip_confirmed_manually'] == summ_df['trip_str']
    ]

    df = f.broadcast(colli_basic_df).join(summ_df, join_cond, 'left') 
    
    return df

def join_colli_summ_driving_with_secbysec_driving(
    colli_with_summ_df: pyspark.sql.DataFrame,
    secbysec_df: pyspark.sql.DataFrame) \
    -> pyspark.sql.DataFrame:
    '''
    Join the collision summary info with sec-by-sec data
    '''
    
    join_cond = [
        colli_with_summ_df['summ_filename'] == secbysec_df['ptt_filename'],
        colli_with_summ_df['trip_str'] == secbysec_df['trip']
    ]
    temp_cols = list(filter(
        lambda x: x not in ('trip', 'ptt_filename', 't_pt_id', 't_trip', 't_geohash'), 
        secbysec_df.columns
    )) + ["geohash"]
    
    df = f.broadcast(
            colli_with_summ_df \
                .select(
                    'temp_id', 'summ_filename', 
                    'trip_str', 'raw_distance', 'raw_duration_cal', 
                    'collision_row_num', 'raw_start_dt', 'raw_end_dt', 'home_gps')) \
        .join(secbysec_df, join_cond, 'left') \
        .filter(~(f.col('summ_filename').isNull())) \
        .withColumn(
            'datetime', 
            f.from_unixtime(
                f.unix_timestamp(f.col('datetime')), 
                format='yyyy-MM-dd HH:mm:ss+100')) \
        .drop('trip', 'ptt_filename') \
        .filter(
            ~(f.col('row_number').isNull())) \
        .withColumn(
            'geohash', f.col('t_geohash')) \
        .groupby(
            'temp_id', 'summ_filename', 
            'trip_str', 'raw_distance', 'raw_duration_cal', 
             'collision_row_num', 'raw_start_dt', 'raw_end_dt', 
             'home_gps') \
        .agg(
            f.collect_list(f.struct(*temp_cols)) \
                .alias('secbysec_col')) \
        .withColumn(
            'secbysec_col', 
            sorted_nested_list_udf(f.col('secbysec_col'))) \
        .withColumn(
            'serial', 
            get_trip_serial_udf(f.col('secbysec_col'), f.lit('serial')))
    
    return df

def extract_colli_details(
    colli_secbysec_details: pyspark.sql.DataFrame) \
    -> pyspark.sql.DataFrame:
    
    df = colli_secbysec_details \
        .withColumn(
            'pre_collision_geohash',
            get_collision_before_after_geohash_udf(
                f.col('secbysec_col'), 
                f.col('collision_row_num').cast(t.LongType()))) \
        .withColumn(
            'collision_moment', 
            get_collision_moment_udf(
                f.col('secbysec_col'),
                f.col('collision_row_num').cast(t.LongType()),
                f.lit('raw_latitude_degree'), 
                f.lit('raw_longititude_degree'),
                f.lit('Parameter_0D_Vehicle_speed_sensor'),
                f.lit('raw_speed_limit_kmh'))) \
        .withColumn(
            'collision_lat',
            f.col('collision_moment').getField('lat_str').cast(t.FloatType())) \
        .withColumn(
            'collision_lon',
            f.col('collision_moment').getField('lon_str').cast(t.FloatType())) \
        .withColumn(
            'home_dist_km', 
            get_dist_from_gps_str_udf(
                f.col('home_gps'), 
                f.concat(
                    f.col('collision_lat').cast(t.StringType()), 
                    f.lit(", "), 
                    f.col('collision_lon').cast(t.StringType())))) \
        .withColumn(
            'start_to_crash_durationinsec',
            get_crash_trip_duration_sec_udf(
                f.col('secbysec_col'), 
                f.col('collision_row_num').cast(t.LongType()))) \
        .withColumn(
            'collision_speed', 
            f.col('collision_moment').getField('speed').cast(t.FloatType())) \
        .withColumn(
            'collision_raw_speed_limit', 
            f.col('collision_moment').getField('speed_limit').cast(t.FloatType())) \
        .drop('collision_moment') \
        .withColumn(
            'gps_hardbreak', 
            get_collision_brake_udf(
                f.col('secbysec_col'), 
                f.col('collision_row_num').cast(t.LongType()),
                f.lit('raw_gps_speed_kmh'))) \
        .withColumn(
            'gps_hardbrake_row_num', 
            f.col('gps_hardbreak').getField('row_num')) \
        .withColumn(
            'gps_hardbrake_speed', 
            f.col('gps_hardbreak').getField('speed_kmh')) \
        .withColumn(
            'gps_hardbrake_acc_kmh_per_s', 
            f.col('gps_hardbreak').getField('deceleration_kmh_per_s')) \
        .drop('gps_hardbreak') \
        .withColumn(
            'vehicle_hardbreak', 
            get_collision_brake_udf(
                f.col('secbysec_col'), 
                f.col('collision_row_num').cast(t.LongType()),
                f.lit('Parameter_0D_Vehicle_speed_sensor'))) \
        .withColumn(
            'vehicle_break_row_num', 
            f.col('vehicle_hardbreak').getField('row_num')) \
        .withColumn(
            'vehicle_break_speed', 
            f.col('vehicle_hardbreak').getField('speed_kmh')) \
        .withColumn(
            'vehicle_deceleration_kmh_per_s', 
            f.col('vehicle_hardbreak').getField('deceleration_kmh_per_s')) \
        .drop('vehicle_hardbreak')
            
    return df

def combine_colli_summ_secbysec_details(
    colli_basic_df: pyspark.sql.DataFrame,
    summ_df: pyspark.sql.DataFrame,
    secbysec_df: pyspark.sql.DataFrame) \
    -> pyspark.sql.DataFrame:
    
    colli_with_summ_df = join_colli_basics_with_summary_driving(
        colli_basic_df, 
        summ_df)
    
    colli_with_secbysec = join_colli_summ_driving_with_secbysec_driving(
        colli_with_summ_df,
        secbysec_df)
    
    df = extract_colli_details(
        colli_with_secbysec)
    
    return df
    
def search_nearby_trips(
    target_collision: pyspark.sql.DataFrame,
    all_secbysec_data: pyspark.sql.DataFrame,
    all_summary_data: pyspark.sql.DataFrame,
    match_threshold: int=2) \
    -> pyspark.sql.DataFrame:
    '''
    Use the collision query to search through the second-by-second data 
    and return the trips Id that passes through the collision areas (matching on geohash)
    using ordered relational division
    '''
    
    # Prepare the repository of all sec-by-sec data by selecting the useful columns
    secbysec_t = all_secbysec_data \
        .select('t_pt_id', 't_trip', 'row_number', 't_geohash')
    
    # Prepare the collisions as query to search through the second-by-second data
    colli_q = target_collision \
        .select(
            f.col('temp_id').alias('q_id'), 
            f.concat(
                f.col('summ_filename'), 
                f.lit("--"),
                f.col('serial'),
                f.lit("--"),
                f.col('trip_str')
            ).alias('q_trip'),
            f.regexp_extract(f.col('temp_id'), "(09-\d{4})_.*", 1).alias('q_pt_id'),
            f.explode(f.col('pre_collision_geohash')).alias('collision_location'),
            f.col('raw_start_dt').alias('q_start_dt'), 
            f.col('raw_end_dt').alias('q_end_dt')) \
        .withColumn(
            'q_row_num', 
            f.col('collision_location').getField('row_number').cast(t.LongType())) \
        .withColumn(
            'q_geohash', 
            f.col('collision_location').getField('geohash')) \
        .drop('collision_location')

    colli_q_count = colli_q \
        .groupby('q_id') \
        .agg(
            f.countDistinct(f.col('q_row_num')) \
                .alias('q_cnt'))
    
    # use inner join to eliminate those trips not passsing the locations
    colli_join_sec_cond = [
        colli_q['q_pt_id'] == secbysec_t['t_pt_id'],
        colli_q['q_geohash'] == secbysec_t['t_geohash'],
        colli_q['q_trip'] != secbysec_t['t_trip']
    ]

    t_join_q = f.broadcast(colli_q) \
        .join(secbysec_t, colli_join_sec_cond, "inner") \
        .select(
            't_trip', 
            't_geohash', 
            f.col('row_number').alias('t_row_num'), 
            'q_row_num', 
            'q_id',
            'q_start_dt', 
            'q_end_dt')

    # prepare for self-join
    t_join_q2 = t_join_q \
        .withColumnRenamed('t_trip', 't2_trip') \
        .withColumnRenamed('t_row_num', 't2_row_num') \
        .withColumnRenamed('q_row_num', 'q2_row_num') \
        .withColumnRenamed('q_id', 'q2_id') \
        .withColumnRenamed('t_geohash', 't2_geohash') \
        .drop('q_start_dt', 'q_end_dt')

    # self-join conditions
    self_join_cond = [
        t_join_q['t_trip'] == t_join_q2['t2_trip'],
        t_join_q['q_id'] == t_join_q2['q2_id']   
    ]

    # use the ordered relation in Ordered Relational Division
    self_join_tq = t_join_q \
        .join(t_join_q2, self_join_cond) \
        .filter( 
            (f.col('t_row_num') < f.col('t2_row_num')) & 
            (f.col('q_row_num') < f.col('q2_row_num')) )

    # use the distinct count step in Ordered Relational Division
    search_result = self_join_tq \
        .groupby(
            f.col('t_trip'), 
            f.col('q_id'), 
            f.col('q_start_dt'), 
            f.col('q_end_dt')) \
        .agg(
            f.countDistinct(f.col('q_row_num')) \
                .alias('join_q_distinct_count')) \
        .join(
            f.broadcast(colli_q_count), 'q_id', 'inner') \
        .filter(
            f.col('join_q_distinct_count') + match_threshold >= f.col('q_cnt'))
    
    df = search_result \
        .join(
            all_summary_data.withColumn(
                't_trip', 
                f.concat(f.col('summ_filename'), 
                f.lit('--'), 
                f.col('serial'), 
                f.lit('--'), 
                f.col('trip_str'))
            ), 
            't_trip', 
            'inner')

    return df

def output_colli_summary_to_parquet(
    colli_details: pyspark.sql.DataFrame,
    filename: str):
    '''
    '''
    colli_details \
        .select(
            f.concat(
                f.col('summ_filename'), f.lit("--"),
                f.col('serial'), f.lit("--"),
                f.col('trip_str')
            ).alias('colli_file_serial_trip_id'),
            f.col('temp_id').alias('colli_id'), 
            f.col('summ_filename').alias('colli_file_pattern'),  
            f.col('collision_row_num').alias('colli_row_number_in_file'), 
            f.col('serial').alias('colli_serial'), 
            f.col('trip_str').alias('colli_trip'),
            f.col('raw_start_dt').alias('colli_raw_start_dt'), 
            f.col('raw_end_dt').alias('colli_raw_end_dt'), 
            f.col('raw_duration_cal').alias('colli_raw_duration_min'),
            f.col('start_to_crash_durationinsec').alias('colli_start_to_crash_sec'),
            f.col('raw_distance').alias('colli_raw_distance'),
            f.col('home_gps').alias('colli_home_gps'),
            f.col('home_dist_km').alias('colli_home_dist_km'),
            f.col('collision_lat').alias('colli_crash_lat'),
            f.col('collision_lon').alias('colli_crash_lon'),
            f.col('collision_speed').alias('colli_crash_speed'),
            f.when(f.col('collision_raw_speed_limit') == 0, -1) \
                .otherwise(f.col('collision_raw_speed_limit')) \
                .alias('colli_crash_speed_limit'),
            f.col('gps_hardbrake_row_num').alias('colli_brake_row_fromgps'),
            f.col('gps_hardbrake_speed').alias('colli_brake_speed_fromgps'),
            f.col('gps_hardbrake_acc_kmh_per_s').alias('colli_brake_acc_kmhpersec_fromgps'),
            f.col('vehicle_break_row_num').alias('colli_brake_row_fromcar'),
            f.col('vehicle_break_speed').alias('colli_brake_speed_fromcar'),
            f.col('vehicle_deceleration_kmh_per_s').alias('colli_brake_acc_kmhpersec_fromcar')) \
        .write.parquet(filename, mode='overwrite')

def output_colli_secbysec_details_to_parquet(
    colli_details: pyspark.sql.DataFrame,
    filename: str):
    
    colli_details \
        .select(
            f.concat(
                f.col('summ_filename'), f.lit("--"),
                f.col('serial'), f.lit("--"),
                f.col('trip_str')
            ).alias('file_serial_trip_id'),
            'temp_id', 
            'secbysec_col') \
        .toDF(
            'colli_file_serial_trip_id', 
            'colli_id', 
            'secbysec_col') \
        .withColumn('secbysec_col', f.explode(f.col('secbysec_col'))) \
        .withColumn('colli_secbysec_filename', f.col('secbysec_col').getField('filename')) \
        .withColumn('colli_secbysec_row_number_in_file', f.col('secbysec_col').getField('row_number')) \
        .withColumn('colli_secbysec_serial', f.col('secbysec_col').getField('serial')) \
        .withColumn('colli_secbysec_keyfob', f.col('secbysec_col').getField('keyfob')) \
        .withColumn('colli_secbysec_datetime', f.col('secbysec_col').getField('datetime')) \
        .withColumn('colli_secbysec_raw_datetime', f.col('secbysec_col').getField('raw_datetime')) \
        .withColumn('colli_secbysec_raw_lat_degree', f.col('secbysec_col').getField('raw_latitude_degree')) \
        .withColumn('colli_secbysec_raw_lon_degree', f.col('secbysec_col').getField('raw_longititude_degree')) \
        .withColumn('colli_secbysec_geohash', f.col('secbysec_col').getField('geohash')) \
        .withColumn('colli_secbysec_speed_fromcar', f.col('secbysec_col').getField('Parameter_0D_Vehicle_speed_sensor')) \
        .withColumn('colli_secbysec_speed_fromgps', f.col('secbysec_col').getField('raw_gps_speed_kmh')) \
        .withColumn('colli_secbysec_raw_speed_limit_kmh', f.col('secbysec_col').getField('raw_speed_limit_kmh')) \
        .withColumn('colli_secbysec_raw_gps_fix_status', f.col('secbysec_col').getField('raw_gps_fix_status')) \
        .withColumn('colli_secbysec_raw_gps_dop_status', f.col('secbysec_col').getField('raw_gps_dop_status' )) \
        .withColumn('colli_secbysec_raw_alerts', f.col('secbysec_col').getField('raw_alerts')) \
        .drop('secbysec_col') \
        .write.parquet(filename, mode='overwrite')
    
def output_ptt_all_trips_summary_to_parquet(
    colli_basics: pyspark.sql.DataFrame,
    summ_df: pyspark.sql.DataFrame, 
    filename: str):
    
    ptt_list = colli_basics \
        .filter(f.col('trip_confirmed_manually').isNotNull()) \
        .select('pt_id') \
        .distinct()
    
    f.broadcast(ptt_list) \
        .join(summ_df, ptt_list.pt_id == summ_df.file_pt_id, 'inner') \
        .select(
            'pt_id', 
            'summ_filename', 'serial', 'trip_str', 
            'raw_start_dt', 'raw_end_dt', 
            'raw_distance') \
        .withColumn(
            'file_serial_trip_id', 
            f.concat(f.col('summ_filename'), 
                     f.lit('--'), 
                     f.col('serial'), 
                     f.lit('--'), 
                     f.col('trip_str'))
        ) \
        .withColumnRenamed('summ_filename', 'file_pattern') \
        .withColumnRenamed('trip_str', 'trip') \
        .select(
            'file_serial_trip_id', 'pt_id', 
            'file_pattern', 'serial', 'trip', 
            'raw_start_dt', 'raw_end_dt', 'raw_distance') \
        .orderBy('file_serial_trip_id') \
        .write.parquet(filename, mode='overwrite')

def output_ptt_nearby_trips_summary_to_parquet(
    colli_nearby_trips: pyspark.sql.DataFrame,
    filename: str):
    
    colli_nearby_trips \
        .select('t_trip', 'file_pt_id', 'filename', 'summ_filename', 'row_number', 'serial', 'keyfob', 'trip_str', 
            'raw_start', 'raw_end', 'raw_duration', 'raw_distance', 
            'last_start', 'last_end', 'raw_start_dt', 'raw_end_dt',  'next_start', 'next_end', 
            'q_id', 'q_start_dt', 'q_end_dt', 'q_cnt', 'join_q_distinct_count') \
        .toDF('file_serial_trip_id', 'pt_id', 'filename', 'file_pattern', 'row_number_in_file', 
          'serial', 'keyfob', 'trip', 'raw_start', 'raw_end', 'raw_duration', 'raw_distance',
          'last_trip_start', 'last_trip_end', 'raw_start_dt', 'raw_end_dt', 'next_trip_start', 'next_trip_end',
          'nearby_colli_id', 'nearby_colli_start_dt', 'nearby_colli_end_dt', 
          'nearby_colli_row_cnt', 'nearby_colli_matched_row_cnt') \
        .orderBy('file_serial_trip_id') \
        .write.parquet(filename, mode='overwrite')
    
def read_parquet(filename: str) \
    -> pd.DataFrame:
    '''
    Read parquet file into pandas dataframe
    '''
    if file_util.is_file(filename + "/_SUCCESS"):
        df = pq.read_table(filename).to_pandas()
        
        return df
    else:
        raise IOError('Parquet file not ready')
        