import pandas as pd
import numpy as np
from collections import OrderedDict

def read_exclusion_date_excel(
    filename: str,
    sheet_name: str,
    column_mapping: OrderedDict) \
    -> pd.DataFrame:
    '''
    Read the exclusion dates from excel files
    '''
    dtypes = {key: object for key in column_mapping.keys()}
    df = pd.read_excel(
            filename, 
            sheet_name=sheet_name, 
            dtype=dtypes) \
        .loc[:, list(column_mapping.keys())] \
        .rename(column_mapping, axis=1) \
        .dropna(axis=0, how='all') \
        .reset_index(drop=True)

    df['pt_id'] = np.where(
        pd.isnull(df['pt_id']),
        np.nan,
        '09-' + df['pt_id'].astype('str').str.extract(r'([0-9]{4}).*'))
    
    return df

def wide_to_long(
    df: pd.DataFrame, 
    id_vars: list, 
    value_vars: list,
    var_name: str, 
    value_name: str) \
    -> pd.DataFrame:
    '''
    Transform the wide format to long format
    '''
    # Transform the wide format to long format using pandas.melt()
    df_long = pd.melt(
        df, 
        id_vars=id_vars, 
        value_vars=value_vars,
        var_name=var_name, 
        value_name=value_name)
    
    # Remove the null entries after transforming to long format
    df_long = df_long[
        pd.notnull(df_long[value_name])
    ] \
        .sort_values(by=['pt_id', 'dates']) \
        .reset_index(drop=True)
    
    return df_long

def validate_patterns(
    df: pd.DataFrame,
    target_col: str,
    date_pat: str,
    time_pat: str,
    remark_pat: str,
    target_col_replacement_pattern: str,
    any_pat: str=r'.*') \
    -> pd.DataFrame:
    '''
    
    Valid patterns 
    e.g 20170506
    e.g 20170506=20170526
    e.g 20120419:1045-1230
    e.g 20130999=20131099(y44 201309 to 201310 holiday with car not driven)
    e.g 20139999:1045-1230(servicing on unknown dates)

    '''
    # Deep copy the objects to avoid side effects
    temp_df = df.copy()
    
    date_from_to_pat = date_pat + '=' + date_pat      # e.g 20170506=20170526
    date_time_pat = date_pat + ':' + time_pat  # e.g 20120419:1045-1230
    
    remark_combinations = date_pat + remark_pat + '$|' + \
        date_from_to_pat + remark_pat + '$|' + \
        date_time_pat + remark_pat + '$'
        
    no_remark_combintations = date_pat + '$|' + \
        date_from_to_pat + '$|' + \
        date_time_pat + '$'

    temp_df['pat_valid'] = np.where(
        temp_df[target_col].str.match(any_pat + remark_pat),
        temp_df[target_col].str.match(remark_combinations),
        temp_df[target_col].str.match(no_remark_combintations))
    
    return temp_df

def extract_remark(
    df: pd.DataFrame,
    remark_pattern: str,
    target_col_replacement_pattern: str,
    target_col: str,
    output_col: str='remark') \
    -> pd.DataFrame:
    '''
    Extract the remark
    '''
    # Deep copy the objects to avoid side effects
    temp_df = df.copy()
    
    # Extract the remark content from the target column
    temp_df[output_col] = temp_df[target_col] \
        .str.extract(remark_pattern, expand=False) \
        .replace(r'^\s*$', np.nan, regex=True) 
    
    # Update the target column to remove the remarks after extraction
    #temp_df[target_col] = temp_df[target_col] \
    #    .str.replace(target_col_replacement_pattern, "") 
    
    return temp_df

def extract_date_time_str(
    df: pd.DataFrame, 
    target_col: str) \
    -> pd.DataFrame:
    '''
    Extract the date and time 
    '''
    # Deep copy the objects to avoid side effects
    temp_df = df.copy()
    
    # Extract the start date of data by 
    # removing the remark in brackets and 
    # replacing the remaining with blank string
    temp_df['start_d'] = temp_df[target_col] \
        .str.replace('[\(].*[\)]', "") \
        .str.replace('=(.*)', "", n=1)
    
    # Extract the end date by  
    # removing the remark in brackets and 
    # replacing the data in front of the delimiter "=" 
    temp_df['end_d'] = temp_df[target_col] \
        .str.replace('[\(].*[\)]', "") \
        .str.replace('(.*)=', "", n=1)
    
    # Extract the start time 
    temp_df['start_t'] = np.where(
        temp_df['start_d'].str.contains(":"),
        temp_df['start_d'].str.replace('(.*):', "", n=1),
        '0000')
    # Extract the end time 
    temp_df['end_t'] = np.where(
        temp_df['start_t'].str.contains("-"),
        temp_df['start_t'].str.replace('(.*)-', "", n=1),
        '2359')
    
    # Further update on start time
    temp_df['start_t'] = np.where(
        temp_df['start_t'].str.contains("-"),
        temp_df['start_t'].str.replace('-(.*)', "", n=1),
        '0000')
    
    # Further update on start day
    temp_df['start_d'] = np.where(
        temp_df['start_d'].str.contains(":"),
        temp_df['start_d'].str.replace(':(.*)', "", n=1),
        temp_df['start_d'])
    
    # Further update on end day
    temp_df['end_d'] = np.where(
        temp_df['end_d'].str.contains(":"),
        temp_df['start_d'],
        temp_df['end_d'])
    
    return temp_df

def convert_datetime(
    df: pd.DataFrame,
    date_col: str,
    time_col: str,
    output_col: str):
    
    temp_df = df.copy()
    
    temp_df[output_col] = pd.to_datetime(
        temp_df[date_col] + ' ' + temp_df[time_col], 
        errors='coerce')
    
    return temp_df


def extract_start_end_datetime(
    df: pd.DataFrame,
    origin_col: str) \
    -> pd.DataFrame:
    '''
    Convert the start and end columns to datetime format
    '''
    temp_df = df.copy()
    
    temp_df['unknown_d'] = temp_df[origin_col] \
        .str.contains('[0-9]{4}9999')
    
    temp_df['single_d'] = ~temp_df[origin_col] \
        .str.contains('=')
    
    temp_df['start_esti_d'] = np.where(
        temp_df['unknown_d'], 
        np.nan,
        np.where(
            ~temp_df['start_d'].str.contains('[0-9]{4}[01][0-9]99$'),
            temp_df['start_d'],
            temp_df['start_d'].str.replace('99$', "01")))
    
    temp_df['end_esti_d'] = np.where(
        temp_df['unknown_d'], 
        np.nan,
        np.where(
            ~temp_df['end_d'].str.contains('[0-9]{4}[01][0-9]99$'),
            temp_df['end_d'],
            np.where(
                temp_df['single_d'],
                temp_df['end_d'].str.replace('99$', "01"),
                temp_df['end_d'].str.replace('99$', "28"))))
    
    temp_df = convert_datetime(
        temp_df, 
        date_col='start_esti_d',
        time_col='start_t',
        output_col='start_dt')
    
    temp_df = convert_datetime(
        temp_df, 
        date_col='end_esti_d',
        time_col='end_t',
        output_col='end_dt')
    
    temp_df['dt_valid'] = (pd.notnull(temp_df['start_dt'])) & \
        (pd.notnull(temp_df['end_dt'])) & \
        (temp_df['start_dt'] <= temp_df['end_dt'])
    
    return temp_df.drop(columns=['unknown_d', 'single_d', 'start_esti_d', 'end_esti_d'])

def extract_patterns(
    df: pd.DataFrame,
    target_col: str) \
    -> pd.DataFrame:
    '''
    Extract patterns including remark and date and time from target column
    '''
    any_pat = r'.*'
    date_number_pat = r'[0-9]{8}'
    date_pat = r'20[01][0-9][0-9]{4}' # e.g 20170506
    time_pat = r'[0-9]{4}-[0-9]{4}'
    remark_pat = r'[\(]([\w\s=:/\-.,!@$~]*)[\)]'  # e.g (y44 201309 to 201310 holiday with car not driven)
    target_col_replacement_pattern=r'[(][\w\s]*[)]'
    
    # Deep copy the objects to avoid side effects
    temp_df = df.copy()
    
    temp_df[target_col] = temp_df[target_col] \
        .str.split(pat=';')

    temp_df = temp_df \
        .explode(target_col) \
    
    temp_df[target_col] = temp_df[target_col] \
        .str.strip() \
        .replace(r'^\s*$', np.nan, regex=True) 
    
    # Remove the null entries after split and explode
    temp_df = temp_df[
        pd.notnull(temp_df[target_col])
    ] \
        .reset_index(drop=True) \
        .sort_values(by=['pt_id', 'dates']) \
    
    temp_df = validate_patterns(
        temp_df, 
        target_col=target_col,
        date_pat=date_pat,
        time_pat=time_pat,
        remark_pat=remark_pat,
        target_col_replacement_pattern=remark_pat)
    
    temp_df = extract_remark(
        temp_df, 
        remark_pattern=remark_pat, #r'[0-9]{8}=?[0-9]{8}?[(]?(.*)[)]?', or date_number_pat + '=?' + date_number_pat + '?' + 
        target_col_replacement_pattern=target_col_replacement_pattern,
        target_col=target_col,
        output_col='remark') 
    
    temp_df = extract_date_time_str(
        temp_df,
        target_col=target_col)
    
    temp_df = extract_start_end_datetime(
        temp_df,
        origin_col=target_col)
    
    return temp_df

def get_exclusion_dates(
    filename: str,
    column_mapping: OrderedDict,
    index_cols: list,
    sheet_name: str=None) \
    -> pd.DataFrame:
    '''
    Get the exclusion dates in long format from the manual input excel file
    '''
    df = read_exclusion_date_excel(filename, sheet_name, column_mapping)
    
    df_long = wide_to_long(
        df, 
        id_vars=index_cols, 
        value_vars=[col for col in list(column_mapping.values()) if col not in index_cols],
        var_name='categories', 
        value_name='dates')
    
    df_extracted = extract_patterns(
        df_long, 
        target_col='dates')
    
    return df_extracted

def check_overlap(
    df: pd.DataFrame,
    groupby_keys: list,
    target_start_col: str,
    target_end_col: str,
    info_cols_for_output: list,
    initial_earliest: pd.Timestamp=pd.Timestamp('1900-01-01')) \
    -> pd.DataFrame: 
    '''
    Compare the start and end columns to see if any period is overlapped
    and add the column of "overlap" with category and dates info
    Reference from # https://binal.pub/2018/09/dealing-with-nested-data-in-pandas/
    '''
    temp_df = df.copy()
    # temp_df['ori_index'] = temp_df.index.astype('str')
    
    # info_cols_for_output = 'ori_index' + info_cols_for_output
    
    df_overlap_nested = temp_df.groupby(groupby_keys).apply(
        check_datetime_overlap_for_group, 
        target_start_col=target_start_col,
        target_end_col=target_end_col,
        info_cols_for_output=info_cols_for_output,
        initial_earliest=initial_earliest
    ) \
        .reset_index() 
    
    df_overlap_nested.columns = groupby_keys + ['result']
   
    df_unnested = df_overlap_nested.explode('result') \
        .dropna(axis=0, how='any')
    
    df_overlap_index = pd.concat([
        df_unnested.drop(columns=groupby_keys + ['result']),
        df_unnested['result'].apply(pd.Series)
    ], axis=1) \
        .set_index('index')
    
    df_overlap = df \
        .join(df_overlap_index)
    
    return df_overlap

def check_datetime_overlap_for_group(
    group: pd.core.groupby.DataFrameGroupBy, 
    target_start_col: str,
    target_end_col: str,
    info_cols_for_output: list,
    initial_earliest: pd.Timestamp=pd.Timestamp('1900-01-01')) \
    -> list:
    '''
    Check the time period overlaps for each group 
    and return the list of dictionary with key 'index' and 'overlap'
    e.g. [{'index': 2, 'overlap': 'yes'}, {'index': 4, 'overlap': 'yes'}]
    Reference from
    # https://stackoverflow.com/questions/10951341/pandas-dataframe-aggregate-function-using-multiple-columns
    # https://codereview.stackexchange.com/questions/144567/verify-if-meeting-datetime-ranges-overlap-in-an-ienumerable-list-of-meetings
    # https://stackoverflow.com/questions/43221208/iterate-over-pandas-dataframe-using-itertuples
    '''
    ordered = group.sort_values(by=[target_start_col])
    prior_end = initial_earliest
    prior_index = -1
    overlaps = []
    prior_overlap = ""
    overlap_group = 0
    
    for row in ordered.itertuples():
        if getattr(row, target_start_col) < prior_end:
            index = getattr(row, 'Index')          
            overlap = str(overlap_group) + "|" + "|".join([ str(getattr(row, col)) for col in info_cols_for_output])
            
            overlaps = overlaps + [
                {'index': prior_index, 'overlap': overlap},
                {'index': index, 'overlap': prior_overlap}]
            
            continue            
            
        overlap_group = overlap_group + 1
        prior_end = getattr(row, target_end_col)
        prior_index = getattr(row, 'Index')
        prior_overlap = str(overlap_group) + "|" + "|".join([ str(getattr(row, col)) for col in info_cols_for_output])
        
        
    return overlaps