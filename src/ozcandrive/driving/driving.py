import pandas as pd
import pyspark
from pyspark.sql import functions as f
from pyspark.sql import SparkSession
from pyspark.sql import types as t

def convert_to_float(
    val: str) \
    -> float:
    '''
    Function to convert to float, if failed, return 0.0
    '''
    try: 
        return float(val)
    except:
        return float(0)
    
def geohash_gps(
    lat_str: str, 
    long_str: str) \
    -> str:
    '''
    Function to convert latitude and longitude to geohash. 
        Precision 8 is used based on the visualisation of the geohash size 
        from website https://www.movable-type.co.uk/scripts/geohash.html

    :param lat_str : str
        Latitude degree in String format
    :param long_str : str
        Longitude degree in String format

    :return
        geohashed value of the latitude and longitude with precision 8
    '''
    import pygeohash as pgh
    geohash = pgh.encode(
        convert_to_float(lat_str), 
        convert_to_float(long_str), 
        precision=8)
    return geohash

geohash_gps_udf = f.udf(geohash_gps, t.StringType())

def sorted_nested_list(sec_list: list) \
    -> list:
    '''
    Function to sort a list
    '''
    sorted_l = sorted(
        sec_list, 
        key=lambda row: row['row_number'])
    
    return sorted_l

sorted_nested_list_udf = f.udf(sorted_nested_list, t.ArrayType(t.StructType([
    t.StructField("row_number", t.LongType(), True),
    t.StructField("filename", t.StringType(), True),
    t.StructField("record", t.StringType(), True),
    t.StructField("serial", t.StringType(), True),
    t.StructField("raw_datetime", t.StringType(), True),
    t.StructField("raw_latitude_degree", t.StringType(), True),
    t.StructField("raw_longititude_degree", t.StringType(), True),
    t.StructField("raw_gps_fix_status", t.StringType(), True),
    t.StructField("raw_gps_dop_status", t.StringType(), True),
    t.StructField('raw_gps_speed_kmh', t.StringType(), True),
    t.StructField("raw_speed_limit_kmh", t.StringType(), True),
    t.StructField("raw_alerts", t.StringType(), True),
    t.StructField("keyfob", t.StringType(), True),
    t.StructField("Parameter_0D_Vehicle_speed_sensor", t.StringType(), True),
    t.StructField("gmt", t.StringType(), True),
    t.StructField("datetime", t.StringType(), True),
    t.StructField("geohash", t.StringType(), True)
])))

def sleep(random_time: float):
    import time
    time.sleep(random_time)

def get_osm_way_id(
    lat: float, 
    long: float, 
    random_sleep: float) \
    -> dict:
    
    sleep(random_sleep)
    base_url = "http://nominatim.openstreetmap.org/"
    target_url = f"{base_url}reverse?format=json&lat={lat}&lon={long}&zoom={16}&addressdetails=1&extratags=1"

    import requests
    try:
        response = requests.get(target_url).json()
        way_id = str(response.get('osm_id'))
        display_name = response.get('display_name')
    except:
        way_id = "NA"
        display_name = "NA"
        
    return {"way_id": way_id, "display_name": display_name}

def get_osm_way_info(
    way_id: int, 
    random_sleep: float) \
    -> str:
    
    sleep(random_sleep)
    
    import requests
    base_url = "https://api.openstreetmap.org/api"
    version = '0.6'
    response = requests.get(f'{base_url}/{version}/way/{way_id}')
    return response.content

def get_road_type(way_xml_response_content: str) \
    -> str:
    import xml.etree.ElementTree as ET
    try:
        root = ET.fromstring(way_xml_response_content)
        return root.findall(".//*[@k='highway']")[0].get('v')
    except:
        return "NA"
    
def get_road_maxspeed(way_xml_response_content: str) \
    -> str:
    import xml.etree.ElementTree as ET
    try:
        root = ET.fromstring(way_xml_response_content)
        return root.findall(".//*[@k='maxspeed']")[0].get('v')
    except:
        return "-1"
    
def distance(
    gps1: tuple, 
    gps2: tuple) \
    -> float:
    '''
    Function to calculate approximate great-circle distance in meter between two gps locations
        from website https://geopy.readthedocs.io/en/stable/#module-geopy.distance

    :param gps1 : tuple
        Tuple of latitude and longitude in float format
    :param gps2 : tuple 
        Tuple of latitude and longitude in float format
    :return
        approximate distance in kilometer between two GPS locations
    '''
    import geopy.distance as gpdistance
    return gpdistance.distance(gps1, gps2).km

def get_dist_from_gps_str(
    gps1: str, 
    gps2: str) \
    -> float:
    try:
        gps1_t, gps2_t = tuple(map(
            lambda each: tuple(map(
                lambda x: float(x.strip()), each.split(","))), 
            [gps1, gps2]
        ))
        dist = distance(gps1_t, gps2_t)
    except:
        dist = -1.0
    return dist

get_dist_from_gps_str_udf = f.udf(get_dist_from_gps_str, t.FloatType())

def add_osm_way_id(
    df: pd.DataFrame, 
    lat_col: str, 
    lon_col: str, 
    output_col: str) \
    -> pd.DataFrame:
    '''
    '''
    df[output_col] = df.apply(
        lambda row: get_osm_way_id(row[lat_col], row[lon_col], 1.1), 
        axis=1)
    return df

def add_osm_way_info(
    df: pd.DataFrame, 
    way_col: str, 
    id_col: str, 
    output_col: str) \
    -> pd.DataFrame:
    ''' 
    '''
    df[output_col] = df.apply(
        lambda row: get_osm_way_info(int(row[way_col].get(id_col)), 1.1), 
        axis=1)
    return df

def add_osm_road_type(
    df: pd.DataFrame, 
    way_col: str, 
    output_col: str) \
    -> pd.DataFrame:
    ''' 
    '''
    df[output_col] = df.apply(lambda row: get_road_type(row[way_col]), axis=1)
    return df

def add_osm_maxspeed(
    df: pd.DataFrame, 
    way_col: str,
    output_col: str) \
    -> pd.DataFrame:
    ''' 
    '''
    df[output_col] = df.apply(lambda row: get_road_maxspeed(row[way_col]), axis=1) \
        .str.extract(r'([-]?\d*).*') \
        .apply(convert_to_float, axis=1)
    return df

def add_osm_way(
    df: pd.DataFrame, 
    lat_col: str, 
    lon_col: str) \
    -> pd.DataFrame:
    '''
    '''
    new_df = df \
        .pipe(
            add_osm_way_id, 
            lat_col=lat_col, 
            lon_col=lon_col, 
            output_col='osm_way') \
        .pipe(
            add_osm_way_info, 
            way_col='osm_way',
            id_col='way_id',
            output_col='response') \
        .pipe(
            add_osm_road_type, 
            way_col='response',
            output_col='osm_road_type') \
        .pipe(
            add_osm_maxspeed,
            way_col='response',
            output_col='osm_maxspeed') \
        .drop(['response'], axis=1)
    return new_df

def add_dist_from_home(
    df: pd.DataFrame, 
    home_gps_col: str, 
    lat_col: str,
    lon_col: str,
    output_col: str) \
    -> pd.DataFrame:
    '''
    '''
    df[output_col] = df.apply(lambda row: round(
        get_dist_from_gps_str(
            row[home_gps_col], 
            str(row[lat_col]) + ", " + str(row[lon_col])), 
        2
    ), axis=1)
    return df

def standardize_raw_speed_limit(
    df: pd.DataFrame, 
    input_col: str,
    output_col: str) \
    -> pd.DataFrame:
    '''
    '''
    df[output_col] = df.apply(lambda row: -1 if row[input_col] == 0 else row[input_col], axis=1)
    return df


def read_secbysec_driving(
    spark: SparkSession,
    filename: str) \
    -> pyspark.sql.DataFrame:
    '''
    Read the secbysec driving data, drop the redundant parameters 
    and return the data as Spark DataFrame
    '''
    import re
    temp_df = spark.read.parquet(filename)
    
    nonparameter_list = list(filter(
        lambda col: not re.match(r'Parameter_0[0-9A-C].*', col), 
        temp_df.columns))
    
    df = temp_df.select(*nonparameter_list) 
    
    return df

def read_summary_driving(
    spark: SparkSession,
    filename: str) \
    -> pyspark.sql.DataFrame:
    '''
    Read the summary driving data
    '''
    df = spark.read.parquet(filename) \
        .withColumn('file_pt_id', 
                    f.regexp_extract(f.col('filename'), r'(\d{4})/(.*)', 1)) \
        .withColumn('file_pt_id', f.concat(f.lit('09-'), f.col('file_pt_id'))) \
        .withColumn('trip_str', f.col('trip').cast(t.StringType())) \
        .withColumn('date_str', f.trim(f.col('start_date'))) \
        .withColumn('summ_filename', 
                    f.regexp_extract(f.col('filename'), r'(^.*)(-summary.csv)(.*)', 1)) \
        .withColumn('summ_filename', 
                    f.lower(f.col('summ_filename'))) \
        .drop('trip')
    
    return df
