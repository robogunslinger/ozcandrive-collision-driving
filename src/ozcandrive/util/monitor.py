import psutil
from datetime import datetime
import time

def main():
    while True:
      dt = datetime.now().isoformat(timespec='seconds')   
      with open('memory_log.txt', 'a+') as f:
        f.write(str(dt) + "," + str(psutil.virtual_memory().used / 1024 / 1024 / 1024) + "\n") 
      time.sleep(2)

if __name__ == "__main__":
  main()


