import os

def is_file(filename: str) \
    -> bool:
    '''
    Check if the given filename is a file by validating that it is not a directory
    '''
    if not os.path.exists(filename):
        return False
    if os.path.isdir(filename):
        return False
    if not os.path.isfile(filename):
        return False
    
    return True