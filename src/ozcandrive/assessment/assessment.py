import pandas as pd
from pathlib import Path
from pyreadstat._readstat_parser import metadata_container
from ozcandrive.util.file_util import is_file

def read_spss_file_list(
    filepath: str='./spss') \
    -> list:
    '''
    Read the list of files within the SPSS folder
    '''
    files = sorted(filter(
        lambda filename: is_file(filename) and is_spss_format(str(filename)), 
        list(Path(filepath).glob('**/*'))))
    return files
    
def is_spss_format(filename: str) \
    -> bool:
    '''
    Check if the given filename is an SPSS file by validating that it has ".sav" extension
    '''
    import re
    # Assume the SPSS files have the extension of ".sav" or ".SAV" at the end of filename
    if re.search(r'\.sav$', filename, flags=re.IGNORECASE):
        return True
    return False

def is_completed_file(filename: str) \
    -> bool:
    '''
    Check if the given file is completed by validating the folder not having "TO BE CHECKED"
    '''
    import re
    # Assume the completed files' folders have the format of "TO BE CHECKED" or "TOBECHECKED"
    if re.search(r'TO ?BE ?CHECKE?D?', filename, flags=re.IGNORECASE):
        return False
    return True

def filter_file(
    type_name: str, 
    from_files: list) \
    -> list:
    '''
    Filter the file types based on the pattern (case-insensitive)
    '''
    import re
    results = [file 
               for file in from_files
               if re.search(r'{}'.format(type_name), file.get('type'), flags=re.IGNORECASE)]
    return results

def get_year_from_filename(filename: str) \
    -> int:
    '''
    Extract the followup year from the file directory
    '''
    import re
    # Assume the year has the format of "...Y1..."
    m = re.search('(?<=Y)[0-9]*', filename)
    try:
        year = int(m.group(0))
    except Exception as e:
        year = -1
    else:
        # Assume the year between 1 and 8
        if year >= 9 or year < 1:
            year = -1
    return year

def get_type_from_filename(filename: str) \
    -> str:
    '''
    Extract the type of SPSS data from the filename
    '''
    import re
    # Assume the type has the format of "...OZSDA..."
    m = re.search('(.*OZ)(\w+)(_.*)', filename)
    try:
        # Assume some types has the format of "annualYear..." after previous step
        if re.search('year', m.group(2), flags=re.IGNORECASE):
            match = re.search('(\w+)(year).*', m.group(2), flags=re.IGNORECASE).group(1).lower()
        else:
            match = m.group(2).lower()
    except:
        match = ""
        
    return match
        
def spss_to_date(spss_date_in_float: float) \
    -> pd.Timestamp:
    '''
    Convert the SPSS datetime format into pandas datetime format
    Note SPSS datetime starts from the start of Gregorian Calendar in 1582
    '''
    return pd.to_datetime(spss_date_in_float/86400-141428, unit='D')

def read_spss_files(
    years: list, 
    from_files: list) \
    -> list:
    '''
    Read from multiple SPSS files and return data in a list
    '''
    results = []
    for file in from_files:
        year = file.get('year')
        if year not in years:
            continue
        filename = str(file.get('filename'))
        file_type = file.get('type')
        df, meta = read_spss(filename)
        results.append({'filename': filename, 'df': df, 'meta': meta, 'type': file_type, 'year': year})
        
    return results

def convert_datetime_on_col(
    df: pd.DataFrame, 
    meta: metadata_container, 
    type_pattern: str=None, 
    col_list: list=None) \
    -> pd.DataFrame:
    '''
    Given the variable type patterns, convert the columns with the same type to datetime
    '''
    import re
    date_var = {var_name: var_type
                for var_name, var_type in meta.original_variable_types.items() 
                if re.search(type_pattern, var_type, flags=re.IGNORECASE)}
    
    temp_df = df.copy()
    for var in date_var:
        temp_df[var] = spss_to_date(temp_df[var])    
        
    return temp_df
    
def read_spss(filename: str) \
    -> (pd.DataFrame, metadata_container):
    '''
    Read one SPSS file with the datetime format converted to pandas format
    '''
    import pyreadstat
    df, meta = pyreadstat.read_sav(filename)
    
    df = convert_datetime_on_col(df, meta, type_pattern='date')
    df = convert_datetime_on_col(df, meta, type_pattern='moyr')
        
    return (df, meta)
    
def remove_colname_year(df: pd.DataFrame) \
    -> pd.DataFrame:
    '''
    Remove the year in the column names
    '''
    import re
    temp_df = df.copy()
    temp_df.columns = [re.sub('[yY][0-9]+', '', col) for col in temp_df.columns]
    return temp_df
    
def combine_df(from_files: list) \
    -> pd.DataFrame:
    '''
    Combine multiple dataframes into one
    '''
    import numpy as np
    if len(from_files) == 0:
        return None
    
    df = from_files[0].get('df').copy()
    year = from_files[0].get('year')
    df['year'] = pd.Series(np.repeat(year, len(df.index)))
    df = remove_colname_year(df)
    
    for i in range(1, len(from_files)):
        year = from_files[i].get('year')
        another_df = from_files[i].get('df').copy()
        another_df['year'] = pd.Series(np.repeat(year, len(another_df.index)))
        another_df = remove_colname_year(another_df)
    
        df = pd.concat(
           [df, another_df], 
           axis=0, 
           ignore_index=True, 
           verify_integrity=True, 
           sort=False)
    return df

def get_df(
    from_files: list,
    type_name: str='annual', 
    years: list=[1, 2, 3, 4, 5, 6, 7, 8]) \
    -> pd.DataFrame:
    '''
    Read multiple SPSS of the same type given the year range, the designated type 
    '''
    files = filter_file(type_name=type_name, from_files=from_files)
    data = read_spss_files(years=years, from_files=files)
    df = combine_df(data)
    
    return df

def get_multiple_df(
    from_files: list, 
    types: list,
    years: list=[1, 2, 3, 4, 5, 6, 7, 8]) \
    -> dict:
    '''
    Read multiple SPSS of different types given the year range, a list of file types
    '''
    result = { 
        type_name: get_df(from_files=from_files, type_name=type_name, years=years) 
        for type_name in types
    }
    
    return result
    
def check_year(
    target_date: pd.Timestamp, 
    reference_dates: list, 
    year_list: list) \
    -> int:
    '''
    Use a list of reference dates to categorise which year the target date belongs to.
    For example, the reference dates are [2000-01-01, 2001-01-05, 2001-12-31].
    If the target date is 1999-12-31 (which is before the 1st reference date), it should return -1;
    If the target date is between 2000-01-01 and 2001-01-04 (inclusive), it should return 1;
    If the target date is between 2001-01-05 and 2001-12-30 (inclusive), it should return 2;
    If the target date is on or after 2001-12-31, it should return 3.
    '''
    from bisect import bisect
    
    if target_date is None or pd.isnull(target_date) :
        return None
    
    i = bisect(reference_dates, target_date)
    return year_list[i]
    

def check_year_closest(
    target_date: str, 
    reference_dates: list, 
    year_list: list) \
    -> int:
    """
    Assumes reference_dates is sorted. Returns closest value to target_date.

    If two referece dates are equally close, return the smallest date.
    
    Reference from https://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
    """
    from bisect import bisect_left
    
    if target_date is None or pd.isnull(target_date) :
        return None
    
    pos = bisect_left(reference_dates, target_date)
    
    if pos == 0:
        return year_list[0]
    if pos == len(reference_dates):
        return year_list[-1]
    
    before = reference_dates[pos - 1]
    after = reference_dates[pos]
    
    if after - target_date < target_date - before:
        # return the after
        return year_list[pos]
    else:
        # return the before
        return year_list[pos - 1]
    
def get_annual_dates(
        annual_df: pd.DataFrame, 
        site: str="AU") \
    -> pd.DataFrame:
    '''
    Extract the annual assessment dates in a dataframe 
    '''
    pivot_df = annual_df[annual_df['Site']==site] \
        .pivot(index='pt_id', columns='year', values='ann_dt')
    
    return pivot_df

def combine_year_info(
    annual_pivot_df: pd.DataFrame, 
    trips_df: pd.DataFrame,
    join_key: str,
    date_col: str = 'raw_start_dt') \
    -> pd.DataFrame:
    '''
    Combine the annual assessment and calendar year information with the trips 
    The two dataframe should have a common join-key column. 
    The trips_df dataframe requires 
    - column date_col in datetime format
    The annual_pivot_df dataframe requires
    - index being participant ID
    - column name being the years in number
    '''
    annual_dates_df = pd.DataFrame({
        'pt_id': annual_pivot_df.index, 
        'annual_dates': 
            list(annual_pivot_df \
                     .apply(lambda each_ptt: list(each_ptt.dropna()), axis=1)), 
        'annual_years': 
            list(annual_pivot_df \
                     .apply(lambda each_ptt: list(each_ptt.dropna().index), axis=1))
    })

    df = trips_df.copy() \
        .merge(
            annual_dates_df, 
            how='left',
            on=join_key)
     
    df['assess_year'] = df \
        .apply(
            lambda trip: check_year(
                target_date=trip[date_col],
                reference_dates=trip['annual_dates'],
                year_list=[0] + trip['annual_years'],
            ), axis=1)
    
    df['assess_year_closest'] = df \
        .apply(
            lambda trip: check_year_closest(
                target_date=trip[date_col],
                reference_dates=trip['annual_dates'],
                year_list=trip['annual_years'],
            ), axis=1)
        
    df['calendar_year'] = df[date_col].map(lambda x: x.year)
    
    return df.drop(['annual_dates', 'annual_years'], axis=1)

def pivot_year(
    df: pd.DataFrame, 
    pivot_id: list, 
    yr_col: str, 
    count_col: str,
    append_str: str) \
    -> pd.DataFrame:
    '''
    Create the pivot count based on the year
    '''
    pivot_yr = df.pivot_table(
        index=pivot_id,
        columns=yr_col,
        values=count_col,
        aggfunc='count')
    
    pivot_yr = pivot_yr.rename(lambda col: "y" + str(col) + append_str, 
                               axis='columns') \
        .reset_index()
    
    pivot_yr.columns = pivot_yr.columns.rename("")
    
    pivot_yr = fill_na(pivot_yr, value=0)
        
    return pivot_yr

def fill_na(
    df: pd.DataFrame, 
    value: object,
    cols: list=None) \
    -> pd.DataFrame:
    '''
    Fill NA with alternative value in the designated columns of a dataframe
    '''
    temp_df = df.copy()
    
    if cols is None:
        cols = temp_df.columns
        
    for col in cols:
        temp_df[col].fillna(value, inplace=True)
    
    return temp_df