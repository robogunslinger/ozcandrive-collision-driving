import unittest
import os

from tests.context import ozcandrive
from ozcandrive.util.file_util import *

class TestMethods(unittest.TestCase):
        
    @classmethod
    def setUpClass(cls):
        cls.TEST_DIR = os.path.dirname(os.path.abspath(__file__ + "/../"))
        
    def test_is_file(self):
        self.assertTrue(is_file(os.path.join(
            self.TEST_DIR, 'fixtures/annual_test_file.sav')))
        
        self.assertFalse(is_file(os.path.join(
            self.TEST_DIR, 'fixtures/annual_test_file.sav.xxx')))