import unittest
import os

from tests.context import ozcandrive
from ozcandrive.assessment.assessment import *
import pandas as pd

class TestMethods(unittest.TestCase):
        
    @classmethod
    def setUpClass(cls):
        TEST_DIR = os.path.dirname(os.path.abspath(__file__ + "/../"))
        
        cls.annual_data = pd.DataFrame({
            'pt_id': ['09-9000'], 
            'Health_dropout': [0.0], 
            'Dropout_Year': [0.0],
            'y1ann_dt': [pd.Timestamp('2010-05-31 00:00:00')], 
            'y1ann_q1_age': [76.0], 
            'y1ann_q1_agegrp': [2.0],
            'y1ann_q1_ymob': [pd.Timestamp('1950-01-01 00:00:00')], 
            'y1ann_q8_hdegree': [4.0],
            'y1ann_q23_primevehi_code': ['876'], 
            'y1ann_q28_medication_drug1_code': [3409.0], 
            'y1ann_comments': ""
        })
        
        cls.combined_annual_data = pd.DataFrame({
            'pt_id': ['09-9000', '09-9000', '09-9000'], 
            'Health_dropout': [0.0, 0.0, 0.0], 
            'Dropout_Year': [0.0, 0.0, 0.0],
            'ann_dt': [pd.Timestamp('2010-05-31 00:00:00'), pd.Timestamp('2010-05-31 00:00:00'), pd.Timestamp('2010-05-31 00:00:00')], 
            'ann_q1_age': [76.0, 76.0, 80.0], 
            'ann_q1_agegrp': [2.0, 2.0, 3.0],
            'ann_q1_ymob': [pd.Timestamp('1950-01-01 00:00:00'), pd.Timestamp('1950-01-01 00:00:00'), pd.Timestamp('1950-01-01 00:00:00')], 
            'ann_q8_hdegree': [4.0, 4.0, None],
            'ann_q23_primevehi_code': ['876', '876', '876'], 
            'ann_q28_medication_drug1_code': [3409.0, 3409.0, None], 
            'ann_comments': ["", "", ""],
            'year': [1, 2, 5]
        })
        
        cls.files = [
            {'filename': os.path.join(TEST_DIR, 'fixtures/annual_test_file.sav'), 'year': 1, 'type': 'annual'},
            {'filename': os.path.join(TEST_DIR, 'fixtures/annual_test_file.sav'), 'year': 2, 'type': 'annual'},
            {'filename': os.path.join(TEST_DIR, 'fixtures/annual_test_file_2.sav'), 'year': 5, 'type': 'annual'},
            {'filename': os.path.join(TEST_DIR, 'fixtures/annual_test_file.sav'), 'year': 2, 'type': 'test'}
        ]

    def test_is_spss_file(self):
        self.assertTrue(is_spss_format("OZPDA_Y2.sav"))
        self.assertTrue(is_spss_format("../test/annual_test_file.sav"))
        self.assertTrue(is_spss_format("../test/annual_test_file.SAV"))
        self.assertFalse(is_spss_format("../test/annual_test_file.sav.xxx"))

    def test_is_completed_file(self):
        self.assertTrue(is_completed_file('../raw_data/spss/Y8 Data Entry/OZannualYear8_MASTER.sav'))
        self.assertFalse(is_completed_file("../raw_data/spss/Y8 Data Entry/TO BE CHECKED/ToBeDelete/OZannual_Y8_MASTER.sav"))
        self.assertFalse(is_completed_file("../raw_data/spss/Y8 Data Entry/to be checked/ToBeDelete/OZannual_Y8_MASTER.sav"))
        self.assertFalse(is_completed_file("../raw_data/spss/Y8 Data Entry/tobechecked/ToBeDelete/OZannual_Y8_MASTER.sav"))
        self.assertFalse(is_completed_file("../raw_data/spss/Y8 Data Entry/tobecheck/ToBeDelete/OZannual_Y8_MASTER.sav"))
        
    def test_filter_file(self):
        self.assertTrue(len(filter_file(type_name='annual', from_files=self.files)), 2)
        self.assertTrue(len(filter_file(type_name='test', from_files=self.files)), 0)
        
    def test_get_year_from_filename(self):
        self.assertEqual(get_year_from_filename('../raw_data/spss/Y8 Data Entry/OZannualYear8_MASTER.sav'), 8)
        self.assertEqual(get_year_from_filename('../raw_data/spss/Y2 Data Entry/OZMoCA_Y3.sav'), 2)
        self.assertEqual(get_year_from_filename('../raw_data/spss/20190920_Ozcandrive_AllYears_collision_factor.sav'), -1)
        self.assertEqual(get_year_from_filename('../raw_data/spss/Y10 Data Entry/OZMoCA_Y3.sav'), -1)
        self.assertEqual(get_year_from_filename('../raw_data/spss/Y0 Data Entry/OZMoCA_Y3.sav'), -1)

    def test_get_type_from_filename(self):
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZannualYear1_MASTER.sav'), 'annual')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZSDA_Y1.sav'), 'sda')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZDHI_Y1.sav'), 'dhi')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZSymptomChecklist_Y1.sav'), 'symptomchecklist')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZRoadSign_Y1.sav'), 'roadsign')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZDCS_Y1.sav'), 'dcs')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZDBQ_Y1.sav'), 'dbq')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZGDS_Y1.sav'), 'gds')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZDBPlus_Y1.sav'), 'dbplus')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZSF36_Y1.sav'), 'sf36')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZWOMAC_Y1.sav'), 'womac')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZPDA_Y1.sav'), 'pda')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZSDF_Y1.sav'), 'sdf')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZMMSE_Y1.sav'), 'mmse')
        self.assertEqual(get_type_from_filename('../raw_data/spss/Y1 Data Entry/OZMoCA_Y1.sav'), 'moca')
        self.assertEqual(get_type_from_filename('../raw_data/spss/'), '')
    
    def test_spss_to_date(self):
        # Conversion between SPSS datetime number and pandas timestamp
        # (pd.Timestamp('2010-01-01 00:00:00').value / 10**9 / 86400.0 + 141428) * 86400.0
        self.assertEqual(spss_to_date(13481683200.0), pd.Timestamp('2010-01-01 00:00:00'))
        self.assertEqual(spss_to_date(13668825600.0), pd.Timestamp('2015-12-07 00:00:00'))
        self.assertEqual(spss_to_date(13797216000.0), pd.Timestamp('2020-01-01 00:00:00'))
        
    def test_read_spss(self):
        df, meta = read_spss(self.files[0].get('filename'))
        self.assertEqual(df.iloc[0, ]['y1ann_dt'], pd.Timestamp('2010-05-31 00:00:00'))
        pd.util.testing.assert_frame_equal(df, self.annual_data)

    def test_read_spss_files(self):
        years = [1]
        annual_files = filter_file(type_name='annual', from_files=self.files)
        result = read_spss_files(years=years, from_files=annual_files)
        
        self.assertEqual(len(result), 1)
        self.assertEqual(len(result[0].get('df')), 1)
        self.assertEqual(result[0].get('type'), 'annual')
        self.assertEqual(result[0].get('year'), 1)
        pd.util.testing.assert_frame_equal(result[0].get('df'), self.annual_data)
        
    def test_combine_df(self):
        years = [1, 2, 5]
        annual_files = filter_file(type_name='annual', from_files=self.files)
        annual_data = read_spss_files(years=years, from_files=annual_files)
        df = combine_df(annual_data)
        
        self.assertEqual(len(df.index), 3)    
        self.assertEqual(len(df.columns), 12)
        pd.util.testing.assert_frame_equal(df, self.combined_annual_data)
        
        # Validate the original dataframe was not changed
        self.assertEqual(len(annual_data[0].get('df').index), 1)
        self.assertEqual(len(annual_data[0].get('df').columns), 11)
        
    def test_get_df(self):
        df = get_df(from_files=self.files, type_name='annual', years=[1, 2, 5])
        
        self.assertEqual(len(df.index), 3)    
        self.assertEqual(len(df.columns), 12)
        pd.util.testing.assert_frame_equal(df, self.combined_annual_data)
        
    def test_get_multiple_df(self):
        result = get_multiple_df(from_files=self.files, types=['annual', 'test'], years=[2, 5])
        
        self.assertIn('annual', result)
        self.assertIn('test', result)
        
        annual_df = result.get('annual')
        self.assertEqual(len(annual_df.index), 2)    
        self.assertEqual(len(annual_df.columns), 12)
        
        test_df = result.get('test')
        self.assertEqual(len(test_df.index), 1)
        self.assertEqual(len(test_df.columns), 12)
        
    def test_check_year(self):
        annual_dates = [pd.Timestamp('2011-05-17 00:00:00'),
                        pd.Timestamp('2012-05-31 00:00:00'),
                        pd.Timestamp('2013-07-31 00:00:00')]
        result = check_year(
            target_date=pd.Timestamp('2010-04-01'), 
            reference_dates=annual_dates, 
            year_list=[0, 1, 2, 3]
        )
        self.assertEqual(result, 0)
        
        result = check_year(
            target_date=pd.Timestamp('2011-05-17'), 
            reference_dates=annual_dates, 
            year_list=[0, 1, 2, 3]
        )
        self.assertEqual(result, 1)
        
        result = check_year(
            target_date=pd.Timestamp('2012-05-17'), 
            reference_dates=annual_dates, 
            year_list=[0, 1, 2, 3]
        )
        self.assertEqual(result, 1)
        
        result = check_year(
            target_date=pd.Timestamp('2015-07-31'), 
            reference_dates=annual_dates, 
            year_list=[0, 1, 2, 3]
        )
        self.assertEqual(result, 3)
        
    def test_check_year_closest(self):
        
        annual_dates = [pd.Timestamp('2011-05-17 00:00:00'),
                        pd.Timestamp('2012-05-31 00:00:00'),
                        pd.Timestamp('2013-07-31 00:00:00')]
        
        result = check_year_closest(
            target_date=pd.Timestamp('2010-04-01'), 
            reference_dates=annual_dates, 
            year_list=[1, 2, 3]
        )
        self.assertEqual(result, 1)
        
        result = check_year_closest(
            target_date=pd.Timestamp('2011-11-23'), 
            reference_dates=annual_dates, 
            year_list=[1, 2, 3]
        )
        self.assertEqual(result, 1)
        
        result = check_year_closest(
            target_date=pd.Timestamp('2012-05-17'), 
            reference_dates=annual_dates, 
            year_list=[1, 2, 3]
        )
        self.assertEqual(result, 2) 
        
    def test_combine_year_info(self):
        
        annual_dates = pd.DataFrame({
            1: [pd.Timestamp('2010-01-01'), pd.Timestamp('2011-02-01')],
            2: [None, None],
            3: [pd.Timestamp('2014-01-01'), None]
        }, index=['09-9000', '09-9100'])
        
        trips = pd.DataFrame({
            'pt_id': ['09-9000', '09-9000', '09-9000', '09-9000', '09-9100'],
            'raw_start_dt': [
                pd.Timestamp('2009-01-01'), 
                pd.Timestamp('2011-12-31'), 
                pd.Timestamp('2012-12-31'),
                pd.Timestamp('2020-01-01'), 
                pd.Timestamp('2012-01-01')]
        })
        
        df = combine_year_info(
            annual_dates, 
            trips, 
            join_key='pt_id',
            date_col='raw_start_dt')
        self.assertEqual(list(df['assess_year']), [0, 1, 1, 3, 1])
        self.assertEqual(list(df['assess_year_closest']), [1, 1, 3, 3, 1])
        self.assertEqual(list(df['calendar_year']), [2009, 2011, 2012, 2020, 2012])
        self.assertEqual(len(df.columns), 5)