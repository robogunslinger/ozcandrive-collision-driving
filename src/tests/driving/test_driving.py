import unittest
import os

from collections import OrderedDict
from tests.context import ozcandrive
from ozcandrive.driving.driving import *
import pandas as pd

class TestMethods(unittest.TestCase):
        
    @classmethod
    def setUpClass(cls):
        TEST_DIR = os.path.dirname(os.path.abspath(__file__ + "/../"))

    def test_convert_to_float(self):
        self.assertEqual(convert_to_float('1.0'), 1.0)
        self.assertEqual(convert_to_float('a'), 0.0)
                   
    def test_geohash_gps(self):
        
        geohash = geohash_gps(*map(lambda x: x.strip(), "-137.001, 145.0002".split(",")))
        self.assertEqual(geohash, 'p0p0581b')
    
    def test_sorted_nested_list(self):
        pass
    
    def test_get_osm_way_id(self):
        lat = -37.786734
        long = 145.318268
        way = get_osm_way_id(lat, long, random_sleep=0.1)
        display_name = way.get('display_name')
        way_id = int(way.get('way_id'))
        
        self.assertEqual(display_name, 
                         'Hull Road, Manchester, Mooroolbark, Montrose, Shire of Yarra Ranges, Victoria, 3765, Australia')
        self.assertEqual(way_id, 5321034)
    
    def test_get_osm_way_info(self):
        xml_response_root = b'<?xml version="1.0" encoding="UTF-8"?>\n<osm version="0.6" generator="CGImap 0.7.5 (31931 thorn-01.openstreetmap.org)" copyright="OpenStreetMap and contributors" attribution="http://www.openstreetmap.org/copyright" license="http://opendatacommons.org/licenses/odbl/1-0/">\n <way id="5321034" visible="true" version="16" changeset="74649704" timestamp="2019-09-19T02:03:38Z" user="Jeff PJ Smith" uid="10210000">\n  <nd ref="26827947"/>\n  <nd ref="1844943934"/>\n  <nd ref="622644019"/>\n  <nd ref="26967682"/>\n  <nd ref="1844943961"/>\n  <nd ref="622644021"/>\n  <nd ref="1844943571"/>\n  <nd ref="1844943589"/>\n  <nd ref="246580591"/>\n  <nd ref="6029867583"/>\n  <nd ref="6029867581"/>\n  <nd ref="1844943673"/>\n  <nd ref="6804867140"/>\n  <nd ref="6806887326"/>\n  <nd ref="26967683"/>\n  <nd ref="6806887325"/>\n  <nd ref="6806887324"/>\n  <nd ref="6806887323"/>\n  <nd ref="26967684"/>\n  <tag k="highway" v="secondary"/>\n  <tag k="maxspeed" v="60"/>\n  <tag k="name" v="Hull Road"/>\n  <tag k="oneway" v="no"/>\n  <tag k="surface" v="asphalt"/>\n </way>\n</osm>\n'
        
        way_id = 5321034
        osm_road_info = get_osm_way_info(way_id, random_sleep=0.1)
        # to be determined
        pass
        #self.assertEqual(osm_road_info, xml_response_root)
    
    def test_get_road_type(self):
        xml_response_root = b'<?xml version="1.0" encoding="UTF-8"?>\n<osm version="0.6" generator="CGImap 0.7.5 (31931 thorn-01.openstreetmap.org)" copyright="OpenStreetMap and contributors" attribution="http://www.openstreetmap.org/copyright" license="http://opendatacommons.org/licenses/odbl/1-0/">\n <way id="5321034" visible="true" version="16" changeset="74649704" timestamp="2019-09-19T02:03:38Z" user="Jeff PJ Smith" uid="10210000">\n  <nd ref="26827947"/>\n  <nd ref="1844943934"/>\n  <nd ref="622644019"/>\n  <nd ref="26967682"/>\n  <nd ref="1844943961"/>\n  <nd ref="622644021"/>\n  <nd ref="1844943571"/>\n  <nd ref="1844943589"/>\n  <nd ref="246580591"/>\n  <nd ref="6029867583"/>\n  <nd ref="6029867581"/>\n  <nd ref="1844943673"/>\n  <nd ref="6804867140"/>\n  <nd ref="6806887326"/>\n  <nd ref="26967683"/>\n  <nd ref="6806887325"/>\n  <nd ref="6806887324"/>\n  <nd ref="6806887323"/>\n  <nd ref="26967684"/>\n  <tag k="highway" v="secondary"/>\n  <tag k="maxspeed" v="60"/>\n  <tag k="name" v="Hull Road"/>\n  <tag k="oneway" v="no"/>\n  <tag k="surface" v="asphalt"/>\n </way>\n</osm>\n'
        road_type = get_road_type(xml_response_root)
        
        self.assertEqual(road_type, 'secondary')
        
    def test_get_road_maxspeed(self):
        xml_response_root = b'<?xml version="1.0" encoding="UTF-8"?>\n<osm version="0.6" generator="CGImap 0.7.5 (31931 thorn-01.openstreetmap.org)" copyright="OpenStreetMap and contributors" attribution="http://www.openstreetmap.org/copyright" license="http://opendatacommons.org/licenses/odbl/1-0/">\n <way id="5321034" visible="true" version="16" changeset="74649704" timestamp="2019-09-19T02:03:38Z" user="Jeff PJ Smith" uid="10210000">\n  <nd ref="26827947"/>\n  <nd ref="1844943934"/>\n  <nd ref="622644019"/>\n  <nd ref="26967682"/>\n  <nd ref="1844943961"/>\n  <nd ref="622644021"/>\n  <nd ref="1844943571"/>\n  <nd ref="1844943589"/>\n  <nd ref="246580591"/>\n  <nd ref="6029867583"/>\n  <nd ref="6029867581"/>\n  <nd ref="1844943673"/>\n  <nd ref="6804867140"/>\n  <nd ref="6806887326"/>\n  <nd ref="26967683"/>\n  <nd ref="6806887325"/>\n  <nd ref="6806887324"/>\n  <nd ref="6806887323"/>\n  <nd ref="26967684"/>\n  <tag k="highway" v="secondary"/>\n  <tag k="maxspeed" v="60"/>\n  <tag k="name" v="Hull Road"/>\n  <tag k="oneway" v="no"/>\n  <tag k="surface" v="asphalt"/>\n </way>\n</osm>\n'
        maxspeed = get_road_maxspeed(xml_response_root)
        
        self.assertEqual(maxspeed, '60')
        
    def test_get_dist_from_gps_str(self):
        gps1 = "-37.0001, 144.0002"
        gps2 = "-37.0006, 144.1"
        
        distance = round(get_dist_from_gps_str(gps1, gps2), 2)
        
        self.assertEqual(distance, 8.88)
        
    def test_read_manual_identified_colli_info(self):
        # df = read_manual_identified_colli_info()
        pass
    
    def test_read_secbysec_driving(self):
        # df = read_secbysec_driving()
        pass
    
    def test_read_summary_driving(self):
        # df = read_summary_driving()
        pass
    
    def test_join_colli_basics_with_summary_driving(self):
        # df = join_colli_basics_with_summary_driving()
        pass
    
    def test_join_colli_summ_driving_with_secbysec_driving(self):
        # df = join_colli_summ_driving_with_secbysec_driving()
        pass
    
    def test_extract_colli_details(self):
        # df
        pass
    
    def test_combine_colli_summ_secbysec_details(self):
        pass
    
    def test_search_nearby_trips(self):
        pass
    
    
if __name__ == '__main__':
    unittest.main(verbosity=2)
