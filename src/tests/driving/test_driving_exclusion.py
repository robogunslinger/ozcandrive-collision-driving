import unittest
import os

from collections import OrderedDict
from tests.context import ozcandrive
from ozcandrive.driving.excluding.excludingdates import *
import pandas as pd

class TestMethods(unittest.TestCase):
        
    @classmethod
    def setUpClass(cls):
        TEST_DIR = os.path.dirname(os.path.abspath(__file__ + "/../"))
        
        cls.exclusion_file = os.path.join(TEST_DIR, 'fixtures/excel_test_file.xlsx')
        
        cls.exclusion_file_columns = OrderedDict()
        cls.exclusion_file_columns['pttId'] = 'pt_id'
        cls.exclusion_file_columns['Remark'] = 'ptt_remark'
        cls.exclusion_file_columns['2b. Holiday / Travel'] = 'Holiday travel'
        cls.exclusion_file_columns['2b. Surgical Procedure'] = 'Surgery'
        cls.exclusion_file_columns['2b. Changes in  health'] = 'Health changes'
        cls.exclusion_file_columns['2b. Accident / collision'] = 'Car accident'
        cls.exclusion_file_columns['2b. No access to vehicle'] = 'No access to vehicle'
        cls.exclusion_file_columns['2b. Seasonal stoppage'] = 'Seasonal stoppage'
        cls.exclusion_file_columns['2b. Driving suspension'] = 'Driving suspension'
        cls.exclusion_file_columns['2b. Other'] = 'Other'
        cls.exclusion_file_columns['3c. period driving another vehicle'] = 'Driving other car'
        cls.exclusion_file_columns['Q 4b other drive'] = 'Other drivers'
        cls.exclusion_file_columns['5b.  Do you know the date(s) that the key fob was shared?'] = 'Shared keyfob'
        cls.exclusion_file_columns['6. car service'] = 'Car service'
        cls.exclusion_file_columns['Driver log'] = 'Secondary drivers'

        cls.exclusion_raw = pd.DataFrame({
            'pt_id': ['09-9000', '09-9002'],
            'ptt_remark': [
                'up to y8',
                'dlog but no dlog found;up to year 812; drive another smaller car regular >3 times (60km/wk)'
            ],
            'Holiday travel': [
                '20101018=20101116;20120101=20120201', 
                '20130999=20139999(y44 201309- 201310 holiday with car not driven);'
            ],
            'Surgery': [
                None,
                '20120131=20120399(y28 201203 surgery is about 6wk at 1/1 Melbourne road, NT)'
            ],
            'Health changes': [None, None],
            'Car accident': [None, None],
            'No access to vehicle': [None, None],
            'Seasonal stoppage': [None, None],
            'Driving suspension': [None, None],
            'Other': [None, None],
            'Driving other car': [None, None],
            'Other drivers': [None, None],
            'Shared keyfob': [None, None],
            'Car service': [None, None],
            'Secondary drivers': ['20100101:1000-1200', None]
        })
        
        cls.exclusion_raw_long = pd.DataFrame({
            'pt_id': [
                '09-9000', '09-9000', '09-9002', '09-9002'
            ],
            'ptt_remark': [
                'up to y8',
                'up to y8',
                'dlog but no dlog found;up to year 812; drive another smaller car regular >3 times (60km/wk)',
                'dlog but no dlog found;up to year 812; drive another smaller car regular >3 times (60km/wk)'
            ],
            'categories': [
                'Secondary drivers', 
                'Holiday travel', 
                'Surgery', 
                'Holiday travel'
            ],
            'dates': [
                '20100101:1000-1200',
                '20101018=20101116;20120101=20120201',
                '20120131=20120399(y28 201203 surgery is about 6wk at 1/1 Melbourne road, NT)',
                '20130999=20139999(y44 201309- 201310 holiday with car not driven);'
            ]
        })
        
        cls.any_pat = r'.*'
        cls.date_number_pat = r'[0-9]{8}'
        cls.date_pat = r'20[01][0-9][0-9]{4}' # e.g 20170506
        cls.time_pat = r'[0-9]{4}-[0-9]{4}'
        cls.remark_pat = r'[\(]([\w\s=:/\-.,!@$~]*)[\)]'  # e.g (y44 201309 to 201310 holiday with car not driven)
        cls.target_col_replacement_pattern=r'[(][\w\s]*[)]'
        
        cls.exclusion_dates = pd.DataFrame({
            'pt_id': ['09-9000', '09-9000', '09-9000', '09-9002', '09-9002'], 
            'ptt_remark': [
                'up to y8', 
                'up to y8',
                'up to y8',
                'dlog but no dlog found;up to year 812; drive another smaller car regular >3 times (60km/wk)',
                'dlog but no dlog found;up to year 812; drive another smaller car regular >3 times (60km/wk)'
            ],
            'categories': [
                'Secondary drivers',
                'Holiday travel',
                'Holiday travel',
                'Surgery',
                'Holiday travel'
            ],
            'dates': [
                '20100101:1000-1200',
                '20101018=20101116',
                '20120101=20120201',
                '20120131=20120399(y28 201203 surgery is about 6wk at 1/1 Melbourne road, NT)',
                '20130999=20139999(y44 201309- 201310 holiday with car not driven)'
            ],
            'pat_valid': [
                True, True, True, True, True
            ],
            'remark': [
                None, 
                None, 
                None, 
                'y28 201203 surgery is about 6wk at 1/1 Melbourne road, NT',
                'y44 201309- 201310 holiday with car not driven'
            ],
            'start_d': [
                '20100101', '20101018', '20120101', '20120131', '20130999'
            ],
            'end_d': [
                '20100101', '20101116', '20120201', '20120399', '20139999'
            ],
            'start_t': [
                '1000', '0000', '0000', '0000', '0000'
            ],
            'end_t': [
                '1200', '2359', '2359','2359', '2359'
            ],
            'start_dt': [
                pd.Timestamp('20100101 1000'),
                pd.Timestamp('20101018 0000'),
                pd.Timestamp('20120101 0000'),
                pd.Timestamp('20120131 0000'),
                None
            ],
            'end_dt': [
                pd.Timestamp('20100101 1200'),
                pd.Timestamp('20101116 2359'),
                pd.Timestamp('20120201 2359'),
                pd.Timestamp('20120328 2359'),
                None
            ],
            'dt_valid': [True, True, True, True, False]
        })
        
    def test_read_exclusion_dates_excel(self):
        
        exclusion_file = self.exclusion_file
        
        df = read_exclusion_date_excel(
                filename=exclusion_file,
                sheet_name='majorCollision28_ptt_date_exclu',
                column_mapping=self.exclusion_file_columns)
        
        self.assertEqual(len(df.index), 2)
        pd.util.testing.assert_frame_equal(df, self.exclusion_raw)

    def test_wide_to_long(self):

        exclusion_file = self.exclusion_file
        
        raw = read_exclusion_date_excel(
                filename=exclusion_file,
                sheet_name='majorCollision28_ptt_date_exclu',
                column_mapping=self.exclusion_file_columns)
        
        index_cols=['pt_id', 'ptt_remark']
        
        df = wide_to_long(
                raw, 
                id_vars=index_cols, 
                value_vars=[col for col in list(self.exclusion_file_columns.values()) 
                            if col not in index_cols],
                var_name='categories', 
                value_name='dates')
        
        self.assertEqual(len(df.index), 4)
        pd.util.testing.assert_frame_equal(df, self.exclusion_raw_long)
    
    def test_validate_patterns(self):
        raw = pd.DataFrame({
            'pt_id': ['09-9000', '09-9000', '09-9000', '09-9002', '09-9002'], 
            'dates': [
                '20100101:1000=1200',
                '20101018=20101116',
                '20120101-20120201',
                '20120131=20120399(y28 201203- surgery is about 6wk at 1/1 Melbourne road, NT)',
                '20130999:1000-1200(y44 201309201310 holiday with car not driven)'
            ]
        })

        df = validate_patterns(
                raw, 
                target_col='dates',
                date_pat=self.date_pat,
                time_pat=self.time_pat,
                remark_pat=self.remark_pat,
                target_col_replacement_pattern=self.target_col_replacement_pattern)
        
        result = raw.copy()
        result['pat_valid'] = pd.Series([False, True, False, True, True])
        
        pd.util.testing.assert_frame_equal(df, result)
    
    def test_extract_remark(self):
        raw = pd.DataFrame({
            'dates': [
                '20100101:1000-1200',
                '20101018=20101116',
                '20120131=20120399(y28 201203- surgery is about 6wk at 1/1 Melbourne road, NT)',
                '20130999:1000-1200(y44 201309201310 holiday with car not driven)'
            ]
        })

        df = extract_remark(
                raw, 
                remark_pattern=self.remark_pat, 
                target_col_replacement_pattern=self.target_col_replacement_pattern,
                target_col='dates',
                output_col='remark')
        
        result = raw.copy()
        result['remark'] = pd.Series([
            None, 
            None,
            'y28 201203- surgery is about 6wk at 1/1 Melbourne road, NT',
            'y44 201309201310 holiday with car not driven'
        ])
        
        pd.util.testing.assert_frame_equal(df, result)

    def test_extract_date_time_str(self):
        raw = pd.DataFrame({
            'dates': [
                '20100101:1000-1200',
                '20101018=20101116',
                '20120131=20120399(y28 201203- surgery is about 6wk at 1/1 Melbourne road, NT)',
                '20130999:1000-1200(y44 201309201310 holiday with car not driven)']})

        df = extract_date_time_str(
                raw, 
                target_col='dates')
        
        result = raw.copy()
        result['start_d'] = pd.Series(['20100101', '20101018', '20120131', '20130999'])
        result['end_d'] = pd.Series(['20100101', '20101116', '20120399', '20130999'])
        result['start_t'] = pd.Series(['1000', '0000', '0000', '1000'])
        result['end_t'] = pd.Series(['1200', '2359', '2359', '1200'])
        
        pd.util.testing.assert_frame_equal(df, result)

    def test_extract_start_end_datetime(self):
        '''
        result['start_dt'] = pd.Series([
            pd.Timestamp('20100101 1000'),
            pd.Timestamp('20101018 0000'),
            pd.Timestamp('20120131 0000'),
            None
        ])
        result['end_dt'] = pd.Series([
            pd.Timestamp('20100101 1200'),
            pd.Timestamp('20101116 2359'),
            None,
            None
        ])
        result['dt_valid'] = pd.Series([True, True, False, False])
        '''
        pass
    
    def test_extract_patterns(self):
        
        df_extract = extract_patterns(
                self.exclusion_raw_long, 
                target_col='dates')
    
        pd.util.testing.assert_frame_equal(df_extract, self.exclusion_dates)

    def test_get_exclusion_dates(self):
        exclusion_file = self.exclusion_file
        
        df =  get_exclusion_dates(
                filename=exclusion_file, 
                column_mapping=self.exclusion_file_columns,
                index_cols=['pt_id', 'ptt_remark'],
                sheet_name='majorCollision28_ptt_date_exclu')
        
        pd.util.testing.assert_frame_equal(df, self.exclusion_dates)
        
    def test_datetime_check_overlap_for_group(self):
        pass
    
    def test_check_overlap(self):
        pass

if __name__ == '__main__':
    unittest.main(verbosity=2)
    
# unittest.main(argv=[''], verbosity=2, exit=False)