import unittest
import os

from tests.context import ozcandrive
from ozcandrive.collision.collision import *

import pandas as pd

class TestMethods(unittest.TestCase):
        
    @classmethod
    def setUpClass(cls):
        TEST_DIR = os.path.dirname(os.path.abspath(__file__ + "/../"))
        
        cls.exclusion_file = os.path.join(TEST_DIR, 'fixtures/excel_test_file.xlsx')
        cls.parquet_file = os.path.join(TEST_DIR, 'fixtures/colli_summary_test_file.parquet')
        
        cls.sample_records = [
            {'row_number': 9, 'speed': 20.3, 'geohash': 'ac', 
             'raw_latitude_degree': '-37.001', 'raw_longitude_degree': '144.001'}, 
            {'row_number': 10, 'speed': 20.3, 'geohash': 'de', 
             'raw_latitude_degree': '-37.002', 'raw_longitude_degree': '144.002'}, 
            {'row_number': 11, 'speed': 20.0, 'geohash': 'cde', 
             'raw_latitude_degree': '-37.004', 'raw_longitude_degree': '144.004'}, 
            {'row_number': 12, 'speed': 10.0, 'geohash': 'cde', 
             'raw_latitude_degree': '-37.005', 'raw_longitude_degree': '144.005'},
            {'row_number': 13, 'speed': 60.0, 'geohash': 'cd', 
             'raw_latitude_degree': '-37.005', 'raw_longitude_degree': '144.005'}
        ]
        
    def test_get_collision_before_after_geohash(self):
        # the row number are also converted into string format
        records = [
            {'row_number': '9', 'geohash': 'ac'}, 
            {'row_number': '10', 'geohash': 'de'}, 
            {'row_number': '11', 'geohash': 'cde'}, 
            {'row_number': '13', 'geohash': 'cd'}]
        
        # assuming collision at time 11
        before_after_collision = get_collision_before_after_geohash(
            records=self.sample_records, 
            record_id=11)
        print(before_after_collision)
        self.assertEqual(before_after_collision, records)
        
    def test_get_next_different(self):
        
        next_diff1 = get_next_different(
            records=self.sample_records, 
            matching={'key': 'geohash', 'value': 'de'}, 
            returning=['row_number', 'geohash'])

        self.assertEqual(next_diff1, {'row_number': 9, 'geohash': 'ac'})
        
        next_diff2 = get_next_different(
            records=self.sample_records, 
            matching={'key': 'geohash', 'value': 'cd'}, 
            returning=['row_number', 'geohash'],
            reverse=True)
        
        self.assertEqual(next_diff2, {'row_number': 12, 'geohash': 'cde'})
    
    def test_get_crash_trip_duration(self):
        trip_duration = get_crash_trip_duration(
            records=self.sample_records, 
            record_id=12)
            
        self.assertEqual(trip_duration, 3)
        
    def test_get_collision_brake(self):
        
        collision_brake = get_collision_brake(
            records=self.sample_records, 
            record_id=11, 
            col_name='speed')
        
        self.assertEqual(
            collision_brake, 
            {'row_num': 11, 'speed_kmh': '20.0', 'deceleration_kmh_per_s': '-10.0'})
        
    def test_get_most_frequent(self):
        
        most_freq = get_most_frequent(
            records=self.sample_records, 
            col_name='geohash')

        self.assertEqual(
            most_freq, 
            'cde')
        
    def test_read_parquet(self):
        
        sample_df = pd.DataFrame({''
            'colli_file_serial_trip_id': ['9000/year4/09_9000_devrem4--1460--4435'],
            'colli_id': ['09-9000_2015-01-29_01'],
            'colli_file_pattern': ['9000/year4/09_9000_devrem4'],
            'colli_row_number_in_file': [216010],
            'colli_serial': ['1460'],
            'colli_trip': ['4435'],
            'colli_raw_start_dt': [pd.Timestamp('2014-10-15 15:37:12')],
            'colli_raw_end_dt': [pd.Timestamp('2014-10-15 15:51:39')],
            'colli_raw_duration_min': [14.467],
            'colli_start_to_crash_sec': [504],
            'colli_raw_distance': [2.89],
            'colli_home_gps': ['-37.0, 145.0'],
            'colli_home_dist_km': [2.54359],
            'colli_crash_lat': [-37.8201],
            'colli_crash_lon': [145.153],
            'colli_crash_speed': [0],
            'colli_crash_speed_limit': [60],
            'colli_brake_row_fromgps': ['216012'],
            'colli_brake_speed_fromgps': ['0.7'],
            'colli_brake_acc_kmhpersec_fromgps': ['-0.7'],
            'colli_brake_row_fromcar': ['216014'],
            'colli_brake_speed_fromcar': ['1.0'],
            'colli_brake_acc_kmhpersec_fromcar': ['-1.0']
        })
        sample_df['colli_home_dist_km']  = sample_df['colli_home_dist_km'].astype('float32')
        sample_df['colli_crash_lat'] = sample_df['colli_crash_lat'].astype('float32')
        sample_df['colli_crash_lon'] = sample_df['colli_crash_lon'].astype('float32')
        sample_df['colli_crash_speed'] = sample_df['colli_crash_speed'].astype('float32')
        sample_df['colli_crash_speed_limit'] = sample_df['colli_crash_speed_limit'].astype('float32')

        df = read_parquet(self.parquet_file)
        pd.util.testing.assert_frame_equal(df, sample_df)
        
if __name__ == '__main__':
    unittest.main(verbosity=2)
    